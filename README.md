# Darts

GB Studio 1.1 sources for my game [Darts](https://murks.itch.io/darts), which I created for [#AdvJam2019](https://jams.gamejolt.io/advjam2019/games).

Portions of the game stemming from the [GB Studio](https://github.com/chrismaltby/gb-studio/blob/develop/LICENSE) engine are © of the orignal author and remain MIT licensed.
This applies to the assets in the ui directory.

The m5x7.ttf font is © Daniel Linssen and is "free to use but attribution appreciated". It was obtained from [https://managore.itch.io/m5x7](https://managore.itch.io/m5x7).

All other assets are © Philipp Überbacher and are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

All code ("visual programming" or "block code") is © Philipp Überbacher and licensed under the [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
